[![pipeline status](https://gitlab.com/3d_immortal/libdtasks/badges/master/pipeline.svg)](https://gitlab.com/3d_immortal/libdtasks/commits/master)

# libdtasks

## About
Minimal tasking library written in the [D programming language][1]. This library
was heavily influenced by [Sean Parent's concurrency talks][2], as well as
Chrome's [task runners][3].

It currently supports a `SingleThreadTaskRunner` that is capable of either 
running tasks on the current thread, or spawning a single new thread on which it
will run any posted tasks asynchronously.

It also supports a `ThreadPoolTaskRunner` that spwans as many threads as the
number of CPUs the runtime machine has, and posted tasks will run asynchronously
on any of those threads with any arbitrary sequence and order.

## Examples
Here's an example of using two task runners, one that is running on the current
thread, and one that spawns a single new thread. This example uses
`postTaskAndReplyWithResult` to run a task that returns a result and invoke a
reply that accepts that result as an argument. The two tasks are run of two
different threads.
```d
int number;
// |runner2| spawns a single new thread.
SingleThreadTaskRunner runner2 = new SingleThreadTaskRunner(
    SingleThreadTaskRunner.Type.kNewThread, null /* no initial task to run */);
// |runner1| runs on the current thread.
new SingleThreadTaskRunner(SingleThreadTaskRunner.Type.kCurrentThread, {
  AbstractTaskRunner runner1 =
      TaskSystem.get().getCurrentThreadTaskRunner();

  // The result type is an `int`.
  runner2.postTaskAndReplyWithResult!int(
      {
        // Task to be run on runner2 on thread2.
        import std.math : sqrt;
        return cast(int) sqrt(400.0f);
      },
      (int result) {
        // Reply to be run on runner1 on thread1.
        assert(result == 20);
        number = result;

        // Shutdown both runners.
        runner2.shutdown();
        runner1.shutdown();
      });
});

assert(number == 20);
```

See `unittests` for more examples.

[1]: http://dlang.org/
[2]: https://github.com/sean-parent/sean-parent.github.io/blob/master/papers-and-presentations.md#title-better-code-concurrency
[3]: https://cs.chromium.org/chromium/src/base/task_runner.h?q=TaskRunner&sq=package:chromium&l=56

## License
MIT