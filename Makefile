.PHONY: docs clean format dfmt

d-build:
	dub build --parallel

r-build:
	dub build --build=release --parallel

r-build-ldc:
	dub build --build=release --compiler=ldc2 --parallel

test:
	dub test

test-ldc:
	dub test --compiler=ldc2

cov-test:
	dub test --coverage

docs:
	dub build pide --build=docs

clean-profile:
	rm ./trace* profilegc.log

clean:
	dub clean --all-packages
	
format:
	find ./src/ -iname "*.d" -exec clang-format -i -style=Chromium {} \;
	
dfmt:
	find ./src/ -iname "*.d" -exec dub run dfmt -- --inplace {} \;