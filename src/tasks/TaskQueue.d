module tasks.TaskQueue;

import core.sync.condition;
import core.sync.mutex;
import tasks.AbstractTaskRunner;

public:
/**
 * Defines a FIFO queue for tracking a runner's tasks.
 */
final class TaskQueue
{
private:
  AbstractTaskRunner.Task[] mTasks;
  Mutex mMutex;
  Condition mConditionVar;
  bool mDone;

public:
  this() @trusted
  {
    mMutex = new Mutex;
    mConditionVar = new Condition(mMutex);
    mDone = false;
  }

  ~this() @safe
  {
  }

  /**
   * Appends |task| to this queue and notifies any blocked threads on a pop.
   */
  void push(AbstractTaskRunner.Task task) @trusted
  {
    mMutex.lock_nothrow();
    appendTaskThreadUnsafe(task);

    // Unlock before notifying to prevent a thread from waking up only to be
    // locked again.
    mMutex.unlock_nothrow();
    mConditionVar.notify();
  }

  /**
   * Removes a task from the front of the queue and returns it in |outTask| by
   * output reference. The calling thread will block if the queue is empty.
   * Returns false if this queue has been shut down, and true if a task was
   * successfully popped.
   */
  bool pop(out AbstractTaskRunner.Task outTask) @trusted
  {
    mMutex.lock_nothrow();

    // Unlock on return.
    scope (exit)
      mMutex.unlock_nothrow();

    while (!mDone && mTasks.length == 0)
      mConditionVar.wait();

    if (mDone)
      return false;

    outTask = popTaskThreadUnsafe();
    return true;
  }

  /**
   * Like |push()| but it will return immediately with |false| if it fails to
   * lock the queue's mutex. Otherwsie it returns |true|.
   */
  bool tryPush(AbstractTaskRunner.Task task) @trusted
  {
    if (!mMutex.tryLock_nothrow())
      return false;

    appendTaskThreadUnsafe(task);

    mMutex.unlock_nothrow();
    mConditionVar.notify();
    return true;
  }

  /**
   * Like |pop()| but the calling thread won't block if the queue is empty.
   * Instead, it will return immediately with |false| if it fails to lock the
   * mutex, the queue is empty, or the queue has been shut down. Otherwise
   * it returns true.
   */
  bool tryPop(out AbstractTaskRunner.Task outTask) @trusted
  {
    if (!mMutex.tryLock_nothrow())
      return false;

    // Unlock on return.
    scope (exit)
      mMutex.unlock_nothrow();

    if (mDone || mTasks.length == 0)
      return false;

    outTask = popTaskThreadUnsafe();
    return true;
  }

  /**
   * Shuts down the queue. It won't be possible to pop tasks off of this queue.
   * It notifies all threads sleeping on a pop to wake up to return nothing.
   */
  void shutdown() @trusted
  {
    mMutex.lock_nothrow();
    mDone = true;
    mMutex.unlock_nothrow();
    mConditionVar.notifyAll();
  }

private:
  void appendTaskThreadUnsafe(AbstractTaskRunner.Task task) nothrow @safe
  {
    assert(task);
    // clang-format off
    mTasks ~= task;
    // clang-format on
  }

  /**
   * This should only be called when it's guaranteed that |mTasks| has at least
   * one element.
   */
  AbstractTaskRunner.Task popTaskThreadUnsafe() @safe
  {
    auto front = mTasks[0];
    mTasks = mTasks[1 .. $];
    return front;
  }
}
