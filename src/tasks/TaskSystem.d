module tasks.TaskSystem;

import core.sync.mutex;
import core.thread;
import std.concurrency;
import tasks.AbstractTaskRunner;

private:
__gshared TaskSystem instance;

shared static this()
{
  instance = new TaskSystem;
}

public:
/**
 * Defines a singleton object that keeps track of all the currently running
 * task runners. A task runner is tracked only between the point its run loop
 * starts running until it ends (not construction and destruction).
 * There can only be one runner per each thread.
 */
class TaskSystem
{
private:
  Mutex mRunnersListMutex;
  AbstractTaskRunner[const Tid] mRunnersByThreadIDs;

public:
   ~this() @safe
  {
  }

  /**
   * Returns the singleton instance.
   */
  static TaskSystem get() @trusted
  {
    return instance;
  }

  /**
   * Returns the task runner associated with the given thread's |tid|. Returns
   * |null| if |tid| is not assocaited with any currently running task runner.
   */
  AbstractTaskRunner getTaskRunnerByThreadId(in Tid tid) @safe
  {
    synchronized (mRunnersListMutex)
    {
      AbstractTaskRunner* runner = (tid in mRunnersByThreadIDs);
      return runner ? *runner : null;
    }
  }

  /**
   * Returns the currently running task runner that is associated with the
   * calling thread. Returns null if the calling thread is not associated with
   * any running task runner.
   */
  AbstractTaskRunner getCurrentThreadTaskRunner() @safe
  {
    return getTaskRunnerByThreadId(thisTid());
  }

  /**
   * Task runners register themselves when they start running on their
   * associated threads.
   */
  package void registerRunner(AbstractTaskRunner runner) @safe
  {
    synchronized (mRunnersListMutex)
    {
      // No duplicate runners registered for the same thread.
      assert(getCurrentThreadTaskRunner() is null);
      mRunnersByThreadIDs[thisTid()] = runner;
    }
  }

  /**
   * Task runners unregister themselves once their run loops exit.
   */
  package void unregisterRunnerForThreadId(in Tid tid) @safe
  {
    synchronized (mRunnersListMutex)
    {
      mRunnersByThreadIDs.remove(tid);
    }
  }

private:
  this() @safe
  {
    mRunnersListMutex = new Mutex;
  }
}
