module tasks.SingleThreadTaskRunner;

import core.thread;
import std.concurrency;
import tasks.AbstractTaskRunner;
import tasks.TaskQueue;
import tasks.TaskSystem;

public:
/**
 * Defines a concrete task runner that will run its tasks on a single thread
 * (either the current or a new on). That tasks are run asynchronously
 * sequentially according to the order in which they were posted to the runner.
 */
class SingleThreadTaskRunner : AbstractTaskRunner
{
private:
  TaskQueue mQueue;

public:
  /**
   * Defines the type of this runner.
   */
  enum Type
  {
    /**
     * Runner will be associated with the current thread. Note that there can
     * only ONE task runner associated with the current thread.
     */
    kCurrentThread,

    /**
     * Runner will spawn a single new thread and run its run loop in it.
     */
    kNewThread,
  }

  /**
   * Constructs the runner according to the given |type| and runs its run loop
   * immediately. |firstTask| is optional if |type| is |kNewThread| since you
   * have a change to post tasks to it later from the creating thread. However,
   * |firstTask| must be provided for |kCurrentThread| runners since it will
   * run immediately with no tasks to execute and will block forever.
   */
  this(Type type, Task firstTask) @trusted
  {
    mQueue = new TaskQueue;
    postTask(firstTask);

    if (type == Type.kNewThread)
      new Thread(&run).start();
    else
      run();
  }

  ~this() @safe
  {
  }

  final override void postTask(Task task) @safe
  {
    if (task)
      mQueue.push(task);
  }

  final override void shutdown() @safe
  {
    mQueue.shutdown();
  }

protected:
  final override void runnerLoop() @trusted
  {
    while (true)
    {
      Task front;
      if (!mQueue.pop(front))
        return;

      front();
    }
  }
}

// +++++++++++++++++
// Unit tests:
// +++++++++++++++++
private @safe unittest
{
  import std.stdio : writeln;

  // ---------------------------------------------------------------------------

  void testTaskRunnersOnTwoThreads()
  {
    int number;
    int value;
    new SingleThreadTaskRunner(SingleThreadTaskRunner.Type.kCurrentThread, {
      AbstractTaskRunner runner = TaskSystem.get()
        .getCurrentThreadTaskRunner();
      runner.postTask({ number += 5; });
      runner.postTask({ number += 20; });
      runner.postTask({
        AbstractTaskRunner runner2 = new SingleThreadTaskRunner(
        SingleThreadTaskRunner.Type.kNewThread, null);
        runner2.postTask({ ++value; });
        runner2.postTask({ ++value; });
        runner2.postTask({ ++value; });
        runner2.postTask({ ++value; });
        runner2.postTask({ assert(value == 4); });
        runner2.postTask({
          // Inside thread 2.
          runner.postTask({
            // Inside thread 1.
            runner2.postTask({
              writeln("Shutting down TR on thread 2 ...");
              // Inside thread 2.
              runner2.shutdown();
            });

            // Inside thread 1.
            runner.postTask({
              writeln("Shutting down TR on thread 1 ...");
              runner.shutdown();

              // The following posted task won't run.
              runner.postTask({ number += 6; });
            });

            // The following posted task won't run.
            runner.postTask({ number += 6; });
          });
        });

      });

      runner.postTask({ number += 6; });
    });

    assert(number == 31);
  }

  // ---------------------------------------------------------------------------

  void testpostTaskAndReplySameThread()
  {
    int number = 5;
    new SingleThreadTaskRunner(SingleThreadTaskRunner.Type.kCurrentThread, {
      AbstractTaskRunner runner = TaskSystem.get()
        .getCurrentThreadTaskRunner();
      runner.postTaskAndReply({
        // The task.
        assert(number == 5);
        number += 13;
      }, {
        // The reply. Is only invoked after the above task is run.
        assert(number == 18);
        number += 2;

        runner.shutdown();
      });
    });

    assert(number == 20);
  }

  // ---------------------------------------------------------------------------

  void testpostTaskAndReplyWithResultSameThread()
  {
    int number = 40;
    new SingleThreadTaskRunner(SingleThreadTaskRunner.Type.kCurrentThread, {
      AbstractTaskRunner runner = TaskSystem.get()
        .getCurrentThreadTaskRunner();
      runner.postTaskAndReplyWithResult!int(() {
        // The task.
        number += 42;
        return number;
      }, (int result) {
        // The reply. Is only invoked after the above task is run.
        assert(result == 82);

        number += 20;

        runner.shutdown();
      });
    });

    assert(number == 102);
  }

  // ---------------------------------------------------------------------------

  void testpostTaskAndReplyTwoThreads()
  {
    int number = 5;
    Tid runner1Tid;
    Tid runner2Tid;
    SingleThreadTaskRunner runner2 = new SingleThreadTaskRunner(
        SingleThreadTaskRunner.Type.kNewThread, {
      runner2Tid = thisTid();
    });
    new SingleThreadTaskRunner(SingleThreadTaskRunner.Type.kCurrentThread, {
      AbstractTaskRunner runner1 = TaskSystem.get()
        .getCurrentThreadTaskRunner();
      runner1Tid = thisTid();

      writeln("TR 1.1");
      assert(number == 5);
      assert(runner1Tid == thisTid());
      number += 5;

      runner2.postTaskAndReply({
        writeln("TR 2.1");
        // Task to be run on runner2 on thread2.
        assert(runner2Tid == thisTid());
        assert(number == 10);
        number += 7;

        runner2.shutdown();
      }, {
        writeln("TR 1.2");
        // Reply to be run on runner1 on thread1.
        assert(runner1Tid == thisTid());
        assert(number == 17);
        number += 4;

        runner1.shutdown();
      });

    });

    assert(number == 21);
  }

  // ---------------------------------------------------------------------------

  void testpostTaskAndReplyWithResultTwoThreads()
  {
    int number;
    Tid runner1Tid;
    Tid runner2Tid;
    SingleThreadTaskRunner runner2 = new SingleThreadTaskRunner(
        SingleThreadTaskRunner.Type.kNewThread, {
      runner2Tid = thisTid();
    });
    new SingleThreadTaskRunner(SingleThreadTaskRunner.Type.kCurrentThread, {
      AbstractTaskRunner runner1 = TaskSystem.get()
        .getCurrentThreadTaskRunner();
      runner1Tid = thisTid();

      writeln("TR_Result 1.1");
      assert(runner1Tid == thisTid());
      assert(number == 0);

      runner2.postTaskAndReplyWithResult!int({
        writeln("TR_Result 2.1");
        // Task to be run on runner2 on thread2.
        assert(runner2Tid == thisTid());

        import std.math : sqrt;

        return cast(int) sqrt(400.0f);
      }, (int result) {
        writeln("TR_Result 1.2");
        // Reply to be run on runner1 on thread1.
        assert(runner1Tid == thisTid());
        assert(result == 20);
        number = result + 2 * result;

        runner2.shutdown();
        runner1.shutdown();
      });
    });

    assert(number == 60);
  }

  // ---------------------------------------------------------------------------

  // Invoke tests.
  testTaskRunnersOnTwoThreads();
  testpostTaskAndReplySameThread();
  testpostTaskAndReplyWithResultSameThread();
  testpostTaskAndReplyTwoThreads();
  testpostTaskAndReplyWithResultTwoThreads();
}
// -----------------
