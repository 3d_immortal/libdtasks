module tasks.ThreadPoolTaskRunner;

import core.atomic;
import core.thread;
import std.concurrency;
import std.parallelism;
import tasks.AbstractTaskRunner;
import tasks.TaskQueue;

public:
/**
 * Defines a concerete task runner that will spwan as many new threads as the
 * runtime machine's concurrency (total CPU count) supports. Posted tasks will
 * run on any of those threads. No two tasks are guaranteed to run on the same
 * thread. Any shared data the posted tasks access must be synchronized to
 * prevent any race conditions.
 */
class ThreadPoolTaskRunner : AbstractTaskRunner
{
private:
  /**
   * Each thread has a preferred queue of tasks.
   */
  TaskQueue[] mQueues;

  /**
   * Used to determine the index of the queue that a task will be attempted to
   * be pushed to next.
   */
  shared(uint) mNextPushIndex;

  /**
   * Used to assign a queue index in |mQueues| to each thread.
   */
  shared(uint) mLastAvailableIndex;

public:
  this() @trusted
  {
    foreach (i; 0 .. totalCPUs)
    {
      // clang-format off
      mQueues ~= new TaskQueue;
      // clang-format on
    }

    // Make sure queues were created before starting the threads.
    foreach (i; 0 .. totalCPUs)
      new Thread(&run).start();
  }

  ~this() @safe
  {
  }

  final override void postTask(Task task) @safe
  {
    // Try pushing tasks to the queue at |mNextPushIndex|, if not possible, try
    // to push to queues of other threads, if not possible, push to or block on
    // the queue at |mNextPushIndex| until notified.
    if (!task)
      return;

    immutable(uint) startAttemptIndex = atomicOp!"+="(mNextPushIndex, 1) - 1;
    foreach (i; 0 .. totalCPUs)
    {
      if (mQueues[(startAttemptIndex + i) % totalCPUs].tryPush(task))
        return;
    }

    mQueues[startAttemptIndex % totalCPUs].push(task);
  }

  final override void shutdown() @safe
  {
    // Shutdown all queues.
    foreach (i; 0 .. totalCPUs)
      mQueues[i].shutdown();
  }

protected:
  final override void runnerLoop() @trusted
  {
    immutable(uint) thisThreadQueueIndex = atomicOp!"+="(mLastAvailableIndex,
        1) - 1;

    // Try popping tasks off of your queue first, if not possible, steal tasks
    // from other queues, if not possible, pop from or block on your queue until
    // notified.
    while (true)
    {
      Task front;
      foreach (i; 0 .. totalCPUs)
      {
        if (mQueues[(thisThreadQueueIndex + i) % totalCPUs].tryPop(front))
          break;
      }

      if (!front && !mQueues[thisThreadQueueIndex].pop(front))
      {
        // The queue was signaled for shutdown.
        return;
      }

      front();
    }
  }
}

// +++++++++++++++++
// Unit tests
// +++++++++++++++++

private @safe unittest
{
  import std.stdio : writeln;

  writeln("totalCPUs = ", totalCPUs);
  shared(int) number;
  immutable(int) targetNumber = 1000_000;
  ThreadPoolTaskRunner runner = new ThreadPoolTaskRunner;
  foreach (i; 0 .. targetNumber)
  {
    runner.postTask({
      immutable(int) currentNumber = atomicOp!"+="(number, 1);

      if (currentNumber == targetNumber)
      {
        writeln("posting tasks to verify and shutdown ...");
        runner.postTaskAndReply({
          immutable(int) finalNumber = atomicLoad(number);
          writeln("final number value = ", finalNumber);
          assert(finalNumber == targetNumber);
        }, { writeln("shutting down ..."); runner.shutdown(); });
      }
    });
  }

  writeln("pushed all tasks ..");
}

// -----------------
