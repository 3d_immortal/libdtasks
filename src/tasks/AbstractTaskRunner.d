module tasks.AbstractTaskRunner;

import std.concurrency;
import std.functional;
import tasks.TaskSystem;

public:
/**
 * Defines an abstract task runner that implements the common behavior between
 * all runners types. Implementations may run their tasks on the current thread,
 * a single new thread, or a thread pool.
 */
abstract class AbstractTaskRunner
{
public:
  /**
   * The type of the task that runners accept.
   */
  alias Task = void delegate();

  /**
   * Adds |task| to the runner to be run asynchronously according to the
   * implementation's algorithm.
   */
  abstract void postTask(Task task) @safe;

  /**
   * Adds |task| to be run inside this runner. When |task| is run to its
   * completion, |reply| will be posted to run on the original runner that
   * called this function. The original runner may be this same runner or a
   * different runner.
   *
   * WARNING: This function MUST only be called from a task running inside
   * a runner, which will be the original runner the |reply| will be posted to.
   */
  final void postTaskAndReply(Task task, Task reply) @safe
  {
    // Record the current thread ID, in order to use it to post the reply
    // on its associated task runner.
    const Tid currentThreadId = thisTid();
    postTask({
      // Run the task. Here we are on the destination runner, possibly on
      // a different thread.
      task();

      // Now, post the reply back on the originating task runner.
      TaskSystem.get().getTaskRunnerByThreadId(currentThreadId).postTask(reply);
    });
  }

  /**
   * Adds |task| which returns a result to be run on this runner. When |task| is
   * run to its completion, the resulting value from |task| will be forwarded to
   * |reply| as an argument. |reply| will be run on the original runner that
   * called this function. The original runner may be this same runner or a
   * different runner.
   *
   * WARNING: This function MUST only be called from a task running inside
   * a runner, which will be the original runner the |reply| will be posted to.
   */
  void postTaskAndReplyWithResult(ResultType)( // clang-format off
      ResultType delegate() task,
      void delegate(ResultType) reply) @safe
  {
    // clang-format on
    // Record the current thread ID, in order to use it to post the reply
    // on its associated task runner.
    const Tid currentThreadId = thisTid();
    postTask({
      // Run the task. Here we are on the destination runner, possibly on
      // a different thread. Store the result of the task.
      ResultType result = task();

      // Now, post a task on the originating task runner that will invoke the
      // |reply| with the result from |task|.
      TaskSystem.get().getTaskRunnerByThreadId(currentThreadId).postTask({
        reply(result);
      });
    });
  }

  /**
   * Shuts down this task runner, ending its run loop even if there are tasks
   * pending to be run.
   */
  abstract void shutdown() @safe;

protected:
  /**
   * Concrete implementations should call this to start the runners loop.
   */
  final void run() @safe
  {
    TaskSystem.get().registerRunner(this);

    runnerLoop();

    // Reaching here means this task runner has shut down. Remove it from the
    // task system.
    TaskSystem.get().unregisterRunnerForThreadId(thisTid());
  }

  /**
   * To be implemented by concrete runners to run their tasks according to
   * their strategy.
   */
  abstract void runnerLoop() @trusted;
} // class AbstractTaskRunner
